package com.example.task6fakegps

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.task6fakegps.databinding.ActivityMainBinding
import com.example.utils.Constants
import com.google.android.gms.maps.GoogleMap.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    lateinit var binding: ActivityMainBinding
    lateinit var gMap: GoogleMap
    lateinit var storage: SharedPreferences
    lateinit var position: LatLng
    private var marker: Marker? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        clearStorage()
    }

    override fun onMapReady(googlemap: GoogleMap) {
        gMap = googlemap
        checkSavedCoordinates()
        setMapListeners()

    }
    // Take data from Shared Preferences
    private fun checkSavedCoordinates() {
        loadCoordinates()
    }
    // set listeners on map
    private fun setMapListeners() {

        gMap.setOnMapClickListener(OnMapClickListener {

            binding.tvMainLatitudeValue.text = it.latitude.toString()
            binding.tvMainLongitudeValue.text = it.longitude.toString()
            position = LatLng(it.latitude, it.longitude)
            createMarker(it.latitude, it.longitude)
        })
    }

    private fun saveCoordinates() {
        storage = getPreferences(MODE_PRIVATE)
        val edit = storage.edit()
        if (this::position.isInitialized) {

            edit.putFloat(Constants.LATITUDE_VALUE, position.latitude.toFloat())
            edit.putFloat(Constants.LONGITUDE_VALUE, position.longitude.toFloat())
            edit.apply()
            Toast.makeText(this, "Coordinate SAVED", Toast.LENGTH_SHORT).show()             // REMOVE
        } else {
            return
        }
    }

    private fun loadCoordinates() {
        storage = getPreferences(MODE_PRIVATE)

        val latData = storage.getFloat(Constants.LATITUDE_VALUE, Constants.DEFAULT_COORDINATE_VALUE)
        val lngData = storage.getFloat(Constants.LONGITUDE_VALUE, Constants.DEFAULT_COORDINATE_VALUE)
        Toast.makeText(this, "Coordinate LOADED", Toast.LENGTH_SHORT).show()                // REMOVE
        binding.tvMainLatitudeValue.text = latData.toString()
        binding.tvMainLongitudeValue.text = lngData.toString()

        if (this::gMap.isInitialized) {
            createMarker(latData.toDouble(), lngData.toDouble())
        }
    }

    private fun createMarker(latitude: Double, longitude: Double) {
        if (marker != null) {
            marker?.isVisible = false
        }
        marker = gMap.addMarker(MarkerOptions()
            .position(LatLng(latitude, longitude))
            .title(getString(R.string.main_default_title_text))
            .snippet(String.format(getString(R.string.main_default_snippet_text),latitude,longitude))
        )
        marker?.showInfoWindow()
        gMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(latitude, longitude)))
    }

    private fun clearStorage() {
        storage = getPreferences(MODE_PRIVATE)
        storage.edit().clear().apply()
    }

    override fun onResume() {
        super.onResume()
        loadCoordinates()
    }

    override fun onPause() {
        super.onPause()
        saveCoordinates()
    }

    override fun onDestroy() {
        super.onDestroy()
        clearStorage()
        Log.d("TAG", "[onDestroy APP]")
    }
}