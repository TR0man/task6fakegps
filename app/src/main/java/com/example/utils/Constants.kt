package com.example.utils

class Constants {

    companion object {
        const val LATITUDE_VALUE = "latitude"
        const val LONGITUDE_VALUE = "longitude"
        const val DEFAULT_COORDINATE_VALUE = 0.0f
    }

}